# **Lab 2**

## **TASK 1:**

In this task we are going to know about simple rules of ubuntu terminal some basic commads and its working.

#### Step 1:

In this step we just create two files :

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\1st.jpg)



#### Step 2:

In this step we write 10 lines in each file beacuse we want to tell you commad for this and also we need this text for debuging.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\10 lines.jpg)



#### Step 3:

In this step we merge two files and when we merge data from one file will be copy to another.This would be used for copy or append one file data to another.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\Merge.jpg)



#### Step 4:

Redirect is same as merge commad now we have to copy data and place it to another file by creating new one using terminal.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\Ridirect-to-newfile.jpg)



#### Step 5:

In this step we use **head** commad to display the first 2 lines in the file.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\Display first 2 lines.jpg)



#### Step 6:

In this step we use **tail** commad to display the last two lines data in the file.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\last 2.jpg)



#### Step 7:

In this step we use **grep** commad to find the data in the specific file.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\find.jpg)



#### Step 8:

In this step we grant the permission of second file to execute group.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\2nd file to group.jpg)



#### Step 9:

In this step we use **chmod commad and u-w** for to denied to write the text in file. 

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\deny write permit to 2nd file.jpg)



#### Step 10:

In this step we use commad to see location:

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\curreent loc.jpg)



#### Step 11:

To see the list of all files we use **Ls** comamd.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\list all files.jpg)



#### Step 12:

Now create folder by using commad **mkdir**:

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\new folder.jpg)



#### Step 13:

Display the current time using **date** commad:

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\time.jpg)



### Step 14:

Echo commad used to dsiplay the text write on the terminal :

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\Thanku.jpg)









## TASK 2:

### Step 1:

In this step we create file using Touch command and write all lab rules in it.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\All contain rules.jpg)



### Step 2:

In this step we allow the file group area, user area and other area with respect to this **rwx r_x r__.**

using **chmod 75**4 commad.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\rwrwrw.jpg)



### Step 3:

In this we first **LS commad** and **append** all these in new file.

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab2\last.jpg)



​                                                                               **Finish Here**