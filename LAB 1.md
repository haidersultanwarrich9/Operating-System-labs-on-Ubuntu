

#               				       					 *Lab 2*





##              Haider Sultan                        2021-CS-85

##              Supervised by                        Numan Shafi

​                                                     

​        										   February 9,2023



### Description :

In this lab we learn the shell script another type of interface on which we can run basic commads of Linux and Ubuntu . It runs on the top of Kernal same like the commad interpreter.Enables users to run services provided by the UNIX/Linux OS. In its simplest form, a series of commands in a file is a shell program that saves having to retype commands to perform common tasks.



### **TASK 1:**

Now we work on the **nano** shell script for more experience on ubuntu commands and how to run the basic programs we learnt a lot from this and now we are capable to do simple programming on ubuntu.

#### i)- 

In this task we simple take two variables and **multiply** them with each other:



![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\1st.jpg)



**Output :**

​       ![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\answer 1.jpg)







#### ii)-

In this task we simply ask the user to input some parameters through **read** function and by declaring variables and then show the message on screen using **echo**





####  ![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\2nd.jpg)



Here is the **answer** of the following input through the user:![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\22nd.jpg)





#### iii)-

Now the problem is that how to find the current location of the current file in which you are working now and want to see its **location**:![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\3rd.jpg)





#### iv)-

Now if you want to see the all files in the current folder just simply write the command **ls -l** then you will be able to see all files :

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\4th.jpg)





#### v)-

Display the current time by simply writing the command **date**:



![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\5th.jpg)



#### vi)-

**Echo** command will be used for the displaying the output :



![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\6th.jpg)







### **TASK 2:**

#### i)-

In this task i used the **airthmetic** operator to apply all and show the specific no i.e your roll no :

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\task 2 2.jpg)





**Another way :**



![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\task2 1.jpg)

**Output :**



![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\task 2 22.jpg)



#### ii)-

In this sample we use **Relational operator** with the if else logic that returns true if its condition would be **right** else it would show the result that message **"Wrong roll no enter right"**:



![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\task2 11.jpg)





**Answer would be:**

![](C:\Users\Administrator\Desktop\Uet\4th semester\OS LAB\Lab 3\anser t2 1.1.jpg)







​    													                       **FINISH HERE :)**